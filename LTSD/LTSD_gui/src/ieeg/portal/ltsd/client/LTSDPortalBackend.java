package ieeg.portal.ltsd.client;

import ieeg.portal.ltsd.shared.LTSDDataset;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("backend")
public interface LTSDPortalBackend extends RemoteService {
	LTSDDataset getStudyFromLogin(String username, String password, 
			String domainURL, String nameOfStudy) throws IllegalArgumentException;
}

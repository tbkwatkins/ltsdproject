package ieeg.portal.ltsd.client;

import ieeg.portal.ltsd.shared.LTSDDataset;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface LTSDPortalBackendAsync {
	
	void getStudyFromLogin(String un, String pw, String dom, String study, 
			AsyncCallback<LTSDDataset> callback) throws IllegalArgumentException;

}

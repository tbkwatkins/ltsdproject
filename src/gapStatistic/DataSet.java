package gapStatistic;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import edu.upenn.cis.db.mefview.services.CountsByLayer;
import edu.upenn.cis.db.mefview.services.TimeSeries;
import edu.upenn.cis.db.mefview.services.TimeSeriesAnnotation;
import edu.upenn.cis.db.mefview.services.TimeSeriesDetails;
import edu.upenn.cis.db.mefview.services.TimeSeriesIdAndDCheck;
import edu.upenn.cis.db.mefview.services.TimeSeriesInterface;
import edu.upenn.cis.db.mefview.services.UnscaledTimeSeriesSegment;

/**
 * @author thomaswatkins
 * 
 * This class can be used to create a collection of Seizure objects from the annotations in a 
 * study on the portal so that the gap statistic calculation can then be performed upon them. This 
 * creates a distance matrix which is a measure of the similarity of each seizure compared to all
 * other seizures included in the Dataset object.
 * 
 * Note, the gap statistic is only a valid comparison within a single patient, during a single
 * recording.
 * 
 *TODO:
 *Proper java behaviour when throwing run time exceptions?
 *static factory method constructor to get independent construction of a seizure object, wait until portal info?
 *static factory method constructor to get independent construction of a DataSet object
 *think about parallelizing sections, candidates: computing LTSDval, making vector, hammingwindow splitting
 *faster fft algorithm
 *change doubles to floats to save on space?
 *Transposing arrays is annoying, format coming in from portal?
 *Error checking brainstorm.
 *
 */
public class DataSet {
	//Globals
	//index included in case this dataset is part of a DataSetCollection object, currently not
	//implemented correctly
	int index;
	String identifier;
	String filePath;
	int numSzChannelsToAnalyze = Integer.MAX_VALUE;
	List<Seizure> seizures;
	//the double array in the arrayList below consists of double[channels][szData]
	double[][] currentSeizure; 
	ArrayList<double[]> szVectorCache;
	double[][] distanceMatrix;
	LTSDCalc ltsdCalc;

	private final static String url = "https://view-qa.elasticbeanstalk.com/services";
	private final static String snapName = "Study 017";
	private final static String userName = "watkinst";
	private final static String password = "Omfdatfotft1608.";



	
	public static void main(String[] args) {
		//Example run
		System.out.println(getSnapshotSzNum(snapName));
		List<Seizure> szList = getSzInIndexRange(snapName, 4, 8);

		DataSet data = new DataSet(0, "Study 005 subset", "Study 005", szList, 2000, 1000);
		
		data.makeMatrixCache();
		data.makeLTSDMatrix();
		data.printDistanceMatrix();
	}
	
	/**
	 * Constructor
	 * @param index
	 * @param identifier
	 * @param filePath
	 * @param szfileNames
	 */
	DataSet (int index, String identifier, String filePath, List<Seizure> seizures, int windowSize, int nOverlap) {
		this.index = index;
		this.identifier = identifier;
		this.filePath = filePath;
		this.seizures = seizures;	
		szVectorCache = new ArrayList<double[]>();
		ltsdCalc = new LTSDCalc(windowSize, nOverlap);
		//Here we need to add the calculation to work out number of channels?
	}
	
	/**
	 * Helper method for finding number of annotations held in a study. At the moment assumes that all
	 * annotations are seizures.
	 * Portal Integration class?
	 *TODO add checking to make sure some annotations are seizures
	 * @param studyName, name of study on portal
	 * @return number of annotations in this timerseres annotation
	 */
	public static int getSnapshotSzNum (String studyName) {
		TimeSeriesInterface tsi = intializeTimeSeriesInterface(url, userName, password);
		String snapID = tsi.getDataSnapshotIdByName(studyName);
		System.out.println("snapID " + snapID);
		List<TimeSeriesAnnotation> szAnnotations = getSzAnnotationList(tsi, snapID);
		return szAnnotations.size();
	}


	/**
	 * 
	 *PORTAL INTEGRATION
	 * Gets all the EEG data for all channels from the portal for the notations with indices between the values
	 * of the parameters "from" and "to" and creates a Seizure object for each notation.
	 *TODO add checking for whether annotations are of seizure type or not.
	 *TODO add a limit on how many annotations can be given at any one time. Be wary of the data limit from portal.
	 * @param snapName, name of the study on the portal from which the annotations
	 * originate and the data will come from.
	 * @param from, start taking data from annotation at this index.
	 * @param to
	 * @throws Runtime Exception if from and to are illegal, ie -ve indices or there aren't any notations
	 * at these indices.  
	 * @return List of seizure objects which can then be used in the gap statistic calculation.
	 */
	public static List<Seizure> getSzInIndexRange (String snapName, int from, int to) {
		//TODO error checking
		TimeSeriesInterface tsi = intializeTimeSeriesInterface(url, userName, password);
		String snapID = tsi.getDataSnapshotIdByName(snapName);
		List<TimeSeriesIdAndDCheck> channelRevIDs = getChannelRevIDs(tsi, snapID);
		List<TimeSeriesAnnotation> szAnnotations = getSzAnnotationList(tsi, snapID);
		int numSz = szAnnotations.size();

		if (from < 0 || to < 0) throw new RuntimeException("Illegal index, can't be less than 0");
		if (to < from) throw new RuntimeException("to is less than from, illegal indexing");
		if (to > numSz) throw new RuntimeException("to is greater than number of seizures, index out of bounds");

		List<Seizure> szList = new ArrayList<Seizure>();
		double startTime, endTime;
		TimeSeriesAnnotation current;
		double[][] currentData;
		for (int i = from; i < to; i++) {
			current = szAnnotations.get(i);
			startTime = szAnnotations.get(i).getStartTimeUutc().doubleValue();
			endTime = szAnnotations.get(i).getEndTimeUutc().doubleValue();
			currentData = getRawDataSingleSz (tsi, snapName, channelRevIDs, current);
			szList.add(new Seizure(i, startTime, endTime, currentData, snapName));
		}
		return szList;
	}

	/**
	 *split function 
	 * Once all data has been correctly initialized a call to this method will calculate the LTSD matrix from the loaded in data
	 * and print it out to the console.
	 */
	public void makeMatrixCache () {
		//TODO what if there is no data here!
		this.numSzChannelsToAnalyze = 16;
		for (Seizure seizure : this.seizures) {

			this.currentSeizure = seizure.data;
			//Here we have the issue that we may want the start and end indices to vary for each
			//seizure file. For now assume they will be the same and worked out as above
			this.addToSzVectorCache(this.currentSeizure);
		}
	}
	
	/**
	 * Creates the distance matrix of values comparing each of the DataSets seizures to all their others.
	 * Uses already seizure vectors already computed for each seizure and held in the DataSet's vector cache.
	 * If the cache is empty or of size one the function will return without computing an LTSD matrix and print
	 * an error message.
	 */
	public void makeLTSDMatrix () {
		System.out.println("Called: makeLTSDMatrix");
		if (this.szVectorCache.size() == 0) {
			System.out.println("No seizure data held in cache, cannot make LTSDmatrix");
			return;
		}
		if (this.szVectorCache.size() == 1) {
			System.out.println("Only one element of seizure data held in vector cache, cannot make meaningful LTSDmatrix");
			return;
		}
		int numSzs = szVectorCache.size();		
		double[][] distanceMatrix =  new double[numSzs][numSzs];
		double [] Sx, Sy;
		double matrixVal;
		for (int i = 0; i < numSzs; i++) {			
			Sx = szVectorCache.get(i);
			for (int j = 0; j < numSzs; j++) {
				Sy = szVectorCache.get(j);
				matrixVal = ltsdCalc.calculateLTSDMatrixVal(Sx, Sy);
				//Fill in bottom triangle
				distanceMatrix[i][j] = matrixVal;
				//Fill in top triangle
				distanceMatrix[j][i] = matrixVal;	
			}
		}
		this.distanceMatrix = distanceMatrix;
	}
	
	/**
	 * Populates a vector containing the power spectral density data in arrays of doubles for each seizure recorded in a single
	 * a DataSet.
	 * @return vectorCache, double[][] of power spectral density data for each seizure from DataSet.
	 */
	public void addToSzVectorCache (double[][] szFileData) {
		System.out.println("Called: makeSzVectorCache");
		//TODO check sizes are all the same. maybe when we read all the files in....
		szVectorCache.add(ltsdCalc.getAllChannelsRawSzVector(szFileData));
	}
	
	/**
	 * Prints precalculated distance matrix for this DataSet's seizures or a notification that one
	 * hasn't been calculated yet.
	 */
	public void printDistanceMatrix () {
		if (this.distanceMatrix == null) {
			System.out.println("Haven't calculated distance matrix for DataSet " + identifier);
		} else {
			System.out.println("Dataset " + this.identifier);
			for (double[] row : distanceMatrix) {
				System.out.println(Arrays.toString(row));
			}
		}
	}
	
	/**
	 * Portal integration
	 * Makes a call to the static factory method to get a new TimeSeriesInterface object. 
	 * At the moment contains a hard coded reference to the tsiProperties directory.
	 * This may need to be updated. Also what to do when
	 * @param url, web URL of server
	 * @param userName
	 * @param password
	 * @throws Runtime Exception if the request was not valid for any reason
	 * @return TimeSeriesInterface object if a valid request
	 */
	public static TimeSeriesInterface intializeTimeSeriesInterface (String url, String userName, String password) {
		//TODO implement a check that factor method worked, or do away with this?
		TimeSeriesInterface tsi = null;
		try {
			tsi = TimeSeriesInterface.newInstance(url, "watkinst", "Omfdatfotft1608", "/Users/thomaswatkins/Documents/workspace/GapStatistic/libraries");
		} catch (UnknownHostException e) {
			if (tsi == null) {
				e.printStackTrace();
				throw new RuntimeException("Bad request, no TimeSeriesInterface Returned");	
			}	
		}
		return tsi;
	}
	
	/**
	 * portal
	 * Helper method to get TimeSeriesIdAndDCheck objects for each channel that is part of seizure.
	 * @param tsi
	 * @param snapID
	 * @return
	 */
	public static List<TimeSeriesIdAndDCheck> getChannelRevIDs (TimeSeriesInterface tsi, String snapID) {
		List<TimeSeriesDetails> channelList = tsi.getDataSnapshotTimeSeriesDetails(snapID);
		List<TimeSeriesIdAndDCheck> channelNames =  new ArrayList<TimeSeriesIdAndDCheck>();
		TimeSeriesIdAndDCheck curRevIDchID;
		for (TimeSeriesDetails tsd : channelList) {
			curRevIDchID = new TimeSeriesIdAndDCheck(tsd.getRevId(), tsd.getDataCheck());
			channelNames.add(curRevIDchID);
		}
		return channelNames;
	}
	

	
	public static List<TimeSeriesAnnotation> getSzAnnotationList (TimeSeriesInterface tsi, String snapID) {
		//Checks for multiple layers removed, see original long code
		TimeSeries[] timeSeriesArray = tsi.getDataSnapshotTimeSeries(snapID);
		String layer = getLayer(tsi, snapID);
		List<TimeSeriesAnnotation> annotationList = tsi.getTsAnnotations(snapID, 0, layer, timeSeriesArray, 0, 100);
		List<TimeSeriesAnnotation> element;
		int startIndex = 100;
		long startTime = 0;
		int elementSize = 1;
		while (elementSize > 0) {
			element = tsi.getTsAnnotations(snapID, startTime, layer, timeSeriesArray, startIndex, 100);
			elementSize = element.size();
			startIndex += 100;
			annotationList.addAll(element);
			startTime =  annotationList.get((annotationList.size() - 1)).getEndTimeUutc() + 1;
		}
		//Clean annotations
		Iterator<TimeSeriesAnnotation> annotationIterator = annotationList.iterator();
		TimeSeriesAnnotation annotation;
		while (annotationIterator.hasNext()) {
			annotation = annotationIterator.next();
			if ( ! annotation.getType().equals("Seizure")) {
				System.out.println("REMOVING NON-SEIZURE ANNOTATION " + annotation.getType());
				annotationIterator.remove();
			}
		}
		return annotationList;
	}
	


	/**
	 * Portal integration
	 * TODO, check the amount of time being analyzed/
	 * @param tsi
	 * @param snapName
	 * @param channelRevIDs
	 * @param annotation
	 * @return
	 */
	public static double[][] getRawDataSingleSz (TimeSeriesInterface tsi, String snapName, List<TimeSeriesIdAndDCheck> channelRevIDs, TimeSeriesAnnotation annotation) {
		String snapID = tsi.getDataSnapshotIdByName(snapName);
		//calculate start
		//Make modifications here, try 30 seconds before and 90 seconds afterwards
		//TODO error checking
		//old code:
//		Long startTime = annotation.getStartTimeUutc();
//		Long endTime = annotation.getEndTimeUutc();
//		Long duration = endTime - startTime;
//		double start =  startTime.doubleValue();
//		double length = duration.doubleValue();
		//New code:
		double start = annotation.getStartTimeUutc() - 8.1191103E7;
		double length = 2 * 8.1191103E7;
		System.out.println("length " + length);

		UnscaledTimeSeriesSegment[][] rawSeizureValues = tsi.getUnscaledTimeSeriesSetRaw(snapID, channelRevIDs, start, length , 1);
		double scalingFactor;
		int[] singleChanRawSzValues;
		int numChan = rawSeizureValues.length;
		System.out.println(numChan);
		int seizureLength = rawSeizureValues[0][0].getSeries().length;
		System.out.println(seizureLength);
		for (int i = 0; i < numChan; i++) {
			System.out.println(rawSeizureValues[i][0].getSeries().length);
		}
		UnscaledTimeSeriesSegment ts = rawSeizureValues[0][0];
		int[] szData = ts.getSeries();
		System.out.println("Series length " + szData.length);
		double[][] scaledSzValues = new double[numChan][seizureLength];
		UnscaledTimeSeriesSegment current;
		for (int i = 0; i < numChan; i++) {
			current = rawSeizureValues[i][0];
			scalingFactor = current.getScale();
			singleChanRawSzValues = current.getSeries();
			for (int j = 0; j < seizureLength; j++) {
				scaledSzValues[i][j] =  ((double) singleChanRawSzValues[j]) * scalingFactor;
			}	
		}
		return scaledSzValues;
	}
	
	/**
	 * portal integration
	 * The layer is required to to request the annotations from the portal. This method is called in get annotations. If we can make it shorter
	 * make it part of getAnnotations?
	 * @param tsi
	 * @param snapID
	 * @return
	 */
	public static String getLayer (TimeSeriesInterface tsi, String snapID) {
		CountsByLayer layers = tsi.getCountsByLayer(snapID);
		Map<String, Long> annotationsMap = layers.getCountsByLayer(); 
		String layer = "default";
		for (String annotation: annotationsMap.keySet()) {
			System.out.println(annotation);
			//Fix this for multiple layers
			layer = annotation;
			if (annotation.equals("Seizure")) {
				//CHECK IF ANNOTATION is of type seizure
				//NB assume annotations present on all channels? Check this? 
				//check type property of annotation is that of Seizure, could be artifact and we don't want to include this
				//ideally have a seizure layer only but not enforced and not present
				System.out.println("Annotation is of type seizure?");
			}
		}
		return layer;		
	}

	
	
//	public static List<Seizure> getSzInTimeRange (String snapName, double from, double to) {
//	TimeSeriesInterface tsi = intializeTimeSeriesInterface(url, userName, password);
//	String snapID = tsi.getDataSnapshotIdByName(snapName);
//	List<TimeSeriesIdAndDCheck> channelRevIDs = getChannelRevIDs(tsi, snapID);
//	List<TimeSeriesAnnotation> szAnnotations = getSzAnnotationList(tsi, snapID);
//	//design decision to all any argument for two, if greater than size of snapshot so be it, just return all seizures.
//	if (from < 0 || to < 0) throw new RuntimeException("Illegal index, can't be less than 0");
//	if (to < from) throw new RuntimeException("to is less than from, illegal indexing");
//	List<Seizure> szList = new ArrayList<Seizure>();
//	double startTime, endTime;
//	TimeSeriesAnnotation current;
//	double[][] currentData;
//	for (int i = 0; i < szAnnotations.size(); i++) {
//		current = szAnnotations.get(i);
//		startTime = current.getStartTimeUutc().doubleValue();
//		endTime = current.getEndTimeUutc().doubleValue();
//		currentData = getRawDataSingleSz (tsi, snapName, channelRevIDs, current);
//		if (startTime > from && endTime < to) {
//			szList.add( new Seizure(i, startTime, endTime, currentData, snapName));
//		}
//	}
//	return szList;
//}


//	public static void testPortal () {
//		TimeSeriesInterface tsi = new TimeSeriesInterface("", url , "watkinst", "Omfdatfotft1608.", "");
//		//Get snapshot inside this object
//		//must already know what dataset you require!
//		String snapID = tsi.getDataSnapshotIdByName(snapName);
//		System.out.println(snapID);
//		//Now we have name of dataset
//
//
//		//Find the number of channels
//		List<TimeSeriesDetails> channelList = tsi.getDataSnapshotTimeSeriesDetails(snapID);
//		//List of new timeSeriesDetails objects, which contain labels of channels  and ref id numsso the number of them is the number of channels
//		System.out.println(channelList.size());//this is number of channels
//		int numChannels = channelList.size();
//		List<Double> voltageConversionFactors =  new ArrayList<Double>();//I think these may be the sample factors we need below?
//		List<Double> sampleRates =  new ArrayList<Double>();//These may also be the sample factors we need below.
//
//		List<String> channelNames =  new ArrayList<String>();
//		for (TimeSeriesDetails tsd : channelList) {
//			System.out.println(tsd.getChannelName());
//			System.out.println(tsd.getRevId());
//			tsd.getChannelName();
//			//TODO ADD timeSeries revIdsnames or timeSeriesDetails revIds?
//			tsd.getTimeSeries().getRevId();
//			voltageConversionFactors.add((Double) tsd.getVoltageConversion());
//			channelNames.add(tsd.getTimeSeries().getRevId());
//			sampleRates.add(tsd.getSampleRate());
//		}
//		System.out.println(sampleRates.size());
//		boolean allSampleRatesSame = true;
//		for (int i = 0; i < sampleRates.size(); i ++) {
//			System.out.println(sampleRates.get(i));
//			for (int j = 0; j < sampleRates.size(); j ++) {
//				if((double) sampleRates.get(i) != (double) sampleRates.get(j)){
//					allSampleRatesSame = false;
//				}
//			}
//		}
//		System.out.println("ALL SAMPLE RATES SAME: " + allSampleRatesSame);
//
//		//NOW Get number of seizures in the dataset: So total number annotations
//		//each dataset has multiple layers, most have 1 but don't assume
//		//need to know which one has the seizures, this needs to be provided by the user
//		//To get a summary of all the annotations you can use getCountsBylayer method of the TimeSeriesInterface
//		CountsByLayer layers = tsi.getCountsByLayer(snapID);
//		System.out.println(tsi.getCountsByLayer(snapID).getClass().toString());
//		Map<String, Long> annotationsMap = layers.getCountsByLayer(); 
//		//List<TimeSeriesAnnotation> annotations = tsi.getDatasetAnnotations(snapID, 100);
//		System.out.println(layers.getCountsByLayer().toString());
//		System.out.println(layers.getCountsByLayer().entrySet().isEmpty());
//		System.out.println(layers.getCountsByLayer().isEmpty());
//		if (annotationsMap.size() == 0) {
//			System.out.println("No annotations present!");
//		}
//		if (annotationsMap.size() == 1) {
//			System.out.println("size 1, does this mean 1 layer of annotations?");
//		}
//
//		
//		//Get the number of seizures, represented by the number of annotations
////		List<TimeSeriesAnnotation> annotationList  = tsi.getTsAnnotations(snapID, 0, layer, channelNames, 0, 100);
////		List<TimeSeriesAnnotation> element;
////		int startIndex = 100;
////		int elementSize = 1;
////		while (elementSize > 0) {
////			//element = tsi.getDatasetAnnotations(snapID, startIndex, 100);
////			elementSize = element.size();
////			System.out.println(element.size());
////			startIndex += 100;
////			annotationList.addAll(element);
////		}
////		System.out.println("Raw annotationList size " + annotationList.size());
////		//Got all the notations, all the seizures and any other stuff
////		//Clean annotations
////		Iterator<TimeSeriesAnnotation> annotationIterator = annotationList.iterator();
////		TimeSeriesAnnotation annotation;
////		while (annotationIterator.hasNext()) {
////			annotation = annotationIterator.next();
////			if ( ! annotation.getType().equals("Seizure")) {
////				System.out.println("REMOVING NON-SEIZURE ANNOTATION " + annotation.getType());
////				annotationIterator.remove();
////			}
////		}
////		System.out.println("Total number of seizures after annotation List cleaning" + annotationList.size());
////		int totalSeizures = annotationList.size();
////
////		//Now get start and endtimes? or just leave them in the annotations until we need them
////
////		//Sanity check
////		//total number of channels, IDnums of channels
////		//total number of seizures, start and end times of each seizure
////
////		String [] channelRevIDs = channelNames.toArray(new String[channelNames.size()]);
////		String layer =  "layer";
////
////		System.out.println(Arrays.toString(channelRevIDs));
////		TimeSeries[] timeSeriesArray = tsi.getDataSnapshotTimeSeries(snapID);
////		
////		int sampleRate = (int) Math.round(sampleRates.get(0)); 
////		System.out.println(sampleRate);
////
////		//calculate start
////		Long startTime = annotationList.get(0).getStartTimeUutc();
////		Long endTime = annotationList.get(0).getEndTimeUutc();
////		System.out.println(startTime);
////		System.out.println(endTime);
////		Long duration = endTime - startTime;
////		System.out.println(duration);
////		double start =  startTime.doubleValue();
////		System.out.println("Start " + start);
////		double length = duration.doubleValue();
////		System.out.println("Length: " + length);
////		System.out.println();
////		String[] singleChannel = new String[1];
////		singleChannel[0] = channelNames.get(0);
////
////		//channelRevIDs
////		//GET DATA FROM ONE SEIZURE
////		UnscaledTimeSeriesSegment[][] rawSeizureValues = tsi.getUnscaledTimeSeriesSetRaw(snapID, channelRevIDs, start, length , 1);
////		System.out.println("blah");
////		System.out.println(rawSeizureValues.length);
////		System.out.println(rawSeizureValues[0].length);
////
////		double scalingFactor;
////		int[] singleChanRawSzValues;
////		int numChan = rawSeizureValues.length;
////		int seizureLength = rawSeizureValues[0][0].getSeries().length;
////		double[][] scaledSzValues = new double[numChan][seizureLength];
////		UnscaledTimeSeriesSegment current;
////		for (int i = 0; i < numChan; i++) {
////			System.out.println(i);
////			current = rawSeizureValues[i][0];
////			scalingFactor = current.getScale();
////			singleChanRawSzValues = current.getSeries();
////			for (int j = 0; j < seizureLength; j++) {
////				scaledSzValues[i][j] =  ((double) singleChanRawSzValues[i]) * scalingFactor ;
////			}	
////		}
////		//print 
////		System.out.println(scaledSzValues.length);
////		System.out.println(scaledSzValues[0].length);
//
//	}

	






//	/**
//	 *TODO factory method, setup to get info from portal
//	 * @param identifier
//	 * @param windowSize
//	 * @param nOverlap
//	 * @param seizures
//	 * @return
//	 */
//	public static DataSet createDataSet (String identifier, int windowSize, int nOverlap, Seizure... seizures) {
//
//		return null;
//	}
//
//
//	/**
//	 * TODO factory method, setup to get info from portal
//	 * @param identifier
//	 * @param windowSize
//	 * @param nOverlap
//	 * @param seizureNames
//	 * @param startTimes
//	 * @param endTimes
//	 * @return
//	 */
//	public static DataSet createDataSet (String identifier, int windowSize, int nOverlap, String[] seizureNames, double[] startTimes, double[] endTimes) {
//
//		return null;
//	}


	/**
	 * TODO factory method, setup to get info from portal
	 * @param startTime
	 * @param endTime
	 * @param fileName
	 * @return
	 */
	//public static Seizure createSeizure (String snapID, double startTime, double endTime, TimeSeriesInterface tsi) {
	//tsi.getUnscaledTimesSeriesRaw.

	//TODO getSeizureTimes - this will return a list of lists indicating the number of seizures as well.
	//fake list of lists always list size 1
	//return null;
	//}





//	/**
//	 * TODO factory method, setup to get info from portal
//	 * @param startTime
//	 * @param endTime
//	 * @param fileName
//	 * @return
//	 */
//	public static Seizure createSeizure (double[] startTime, double[] endTime, String[] fileName) {
//
//		return null;
//	}


//	/**
//	 * Gets the raw two dimensional array of doubles representing the seizure in a the format seizure[channels][channelData] and sets the instance variable currentSeizure to equal this.
//	 * @param fileName the name of the file containing the seizure data.
//	 */
//	public void getSingleSeizureRawData (String fileName) {
//		String fullFilePath = this.filePath + "/" + fileName;
//		try {
//			//GET SEIZURE INFO FROM PORTAL
//			currentSeizure = FileIO.getSingleRawSeizureData(fullFilePath, true);
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (RuntimeException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}	
//	}



//	/**
//	 * Finds minimum number of channels used in all of the seizure files related to this DataSet and sets
//	 * the field in the DataSet object to this value.
//	 */
//	public void getMinNumChannels () {
//		int minChannels = Integer.MAX_VALUE;
//		String fullFilePath;
//		int fileChannels = Integer.MAX_VALUE;
//		for (Seizure seizure : seizures) {
//			fullFilePath = filePath + "/" + seizure.fileName;
//			System.out.println("Checking number of channels in " + seizure.fileName + "...");
//			try {
//				fileChannels = FileIO.getNumberOfChannelsFromSzFile(fullFilePath);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			System.out.println("Number of channels: " + fileChannels);
//			minChannels = Math.min(minChannels, fileChannels);
//		}
//		this.numSzChannelsToAnalyze = minChannels;
//		System.out.println("DataSet " + identifier + " channels used in seizure analyis: " + minChannels);
//	}

//	/**
//	 * With the portal shouldn't need to clean the data like this.
//	 * Uses the minimum number of channels as well as the seizure start and end times to cut the currentSeizure matrix
//	 * down from its full recording size to just the portion that will be analyzed.
//	 * @param ictalStartIndex
//	 * @param ictalEndIndex
//	 */
//	public void cleanSingleSeizureRawData (int ictalStartIndex, int ictalEndIndex) {
//		if (ictalEndIndex < ictalStartIndex) {
//			throw new RuntimeException("ictalEndIndex is less than ictalStartIndex!");
//		}
//		if (ictalStartIndex < 0) {
//			throw new RuntimeException("ictalStartIndex is negative!");
//		}
//		if (ictalEndIndex > currentSeizure[0].length - 1) {
//			throw new RuntimeException("ictalEndIndex is greater than size of seizure data");
//		}
//		//Problem of which particular channels need to be got rid of? Ignored for now
//		//Create new double array of the correct size
//		if (this.numSzChannelsToAnalyze == Integer.MAX_VALUE) {
//			throw new RuntimeException("Number of channels to analyze yet to be initialized");
//		}
//		//Assumption that all channels have the same amount of data, must do because of matlab not having
//		//jagged arrays
//		double[][] actualSeizure = new double[numSzChannelsToAnalyze][ictalEndIndex - ictalStartIndex + 1];
//		for (int i = 0; i < numSzChannelsToAnalyze; i++) {
//			actualSeizure[i] = Arrays.copyOfRange(currentSeizure[i], ictalStartIndex, ictalEndIndex);
//		}
//		this.currentSeizure = actualSeizure;
//	}





	
//	/**
//	 * Takes the start time of the notation and returns an end time 90 seconds after this. Takes into account the correct
//	 * correct sampling rates for this particular dataset.
//	 * @return
//	 */
//	public static double getAdjustedEndTime () {
//		
//		return 0;
//	}

//	/**
//	 *TODO not used or finished
//	 * Takes the start time of the notation and returns a start time 30 seconds before this. Takes into account the 
//	 * correct sampling rates for this particular dataset. 
//	 * @return
//	 */
//	public static double getAdjustedStartTime (TimeSeriesInterface tsi, String snapName, double start) {
//		String snapID = tsi.getDataSnapshotIdByName(snapName);
//		List<TimeSeriesDetails> channelList = tsi.getDataSnapshotTimeSeriesDetails(snapID);
//		List<Double> sampleRates =  new ArrayList<Double>();
//		//TODO check whether any of these things do not return the correct objects
//		for (TimeSeriesDetails tsd : channelList) {
//			sampleRates.add(tsd.getSampleRate());
//		}
//		System.out.println(sampleRates.size());
//		boolean allSampleRatesSame = true;
//		for (int i = 0; i < sampleRates.size(); i ++) {
//			System.out.println(sampleRates.get(i));
//			for (int j = 0; j < sampleRates.size(); j ++) {
//				if((double) sampleRates.get(i) != (double) sampleRates.get(j)){
//					allSampleRatesSame = false;
//				}
//			}
//		}
//		System.out.println("ALL SAMPLE RATES SAME: " + allSampleRatesSame + sampleRates.get(0));
//		//Need to find sampling rate
//		//work out relation to double we will return..
//		return 0;
//
//	}

}

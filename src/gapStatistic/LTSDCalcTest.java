package gapStatistic;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author thomaswatkins
 *
 */
public class LTSDCalcTest {
	LTSDCalc ltsdCalc;
	double[] medianEven;
	double[] medianOdd;
	double[] medianSingle;
	double[] medianEmpty;
	
	double[] medAbsDev1;
	double[] medAbsDev2;
	
	double[] hammingWindowCheck;
	
	double[] fftTest1;
	double[] fftTest2;
	double[] results;
	double[] expectedTest2;
	double[] fftTest3;
	double[] fftTest4;
	
	MatlabHandler mh =  new MatlabHandler();
	
	double[][] Sx;
	double[][] Sy;
	double[] Sx1D;
	double[] Sy1D;
	
	double[][] Sx2;
	double[][] Sy2;
	double[] Sx21D;
	double[] Sy21D;
	
	double[][] Sx3;
	double[][] Sy3;
	double[] Sx31D;
	double[] Sy31D;
	
	
	@Before
	public void setUp() throws Exception {
		//TODO handle empty arrays being passed.
		//TODO check original order maintained.
		medianEmpty = new double[]{};
		medianSingle = new double[]{5};
		medianEven = new double[]{1,2,3,4,5,6,7,8,9,10};
		medianOdd = new double[]{1,2,3,4,5,6,7,8,9,10,11};
		
		ltsdCalc = new LTSDCalc(2000, 1000);
		//TODO check original order maintained.
		medAbsDev1 = new double[]{1, 1, 2, 2, 4, 6, 9};
		medAbsDev2 = new double[]{3, 5, 5, 6, 6, 6, 8, 90};
		
		
		hammingWindowCheck =  new double[]{0.0357, 0.2411, 0.4464, 0.2411, 0.0357};
		
		//FFT checks
		fftTest1 = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
		fftTest2 = new double[]{0,1,2,3,4,5,6,7};
		//initialize array for results as function doesn't return but works in place
		results =  new double[]{0,0,0,0,0,0,0,0};
		expectedTest2 = new double[]{28.0, 10.4525, 5.6568, 4.3295, 0.0, 0.0, 0.0, 0.0};
		
		
		//LTSD CALC test
		Sx = MatlabHandler.getKnownMatMatrix("Sx", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sx.mat");
		Sy = MatlabHandler.getKnownMatMatrix("Sy", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sy.mat");
		Sx1D = LTSDCalc.twoDToOneD(Sx);
		Sy1D = LTSDCalc.twoDToOneD(Sy);
		
		Sx2 = MatlabHandler.getKnownMatMatrix("Sx", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sx2.mat");
		Sy2  = MatlabHandler.getKnownMatMatrix("Sy", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sy2.mat");
		Sx21D = LTSDCalc.twoDToOneD(Sx2);
		Sy21D = LTSDCalc.twoDToOneD(Sy2);
		
		Sx3 = MatlabHandler.getKnownMatMatrix("Sx", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sx3.mat");
		Sy3 = MatlabHandler.getKnownMatMatrix("Sy", "/Users/thomaswatkins/Documents/fourier/testData/test1/Sy3.mat");
		Sx31D = LTSDCalc.twoDToOneD(Sx3);
		Sy31D = LTSDCalc.twoDToOneD(Sy3);
	}

	@Test
	public void testGetMedian() {
		assertEquals(LTSDCalc.getMedian(medianOdd), (double) 6, 0.001);
		assertEquals(LTSDCalc.getMedian(medianEven), (double) 5.5, 0.001);
		assertEquals(LTSDCalc.getMedian(medianSingle), (double) 5, 0.001);
		assertEquals(LTSDCalc.getMedian(medAbsDev2), (double) 6, 0.001);

		//TODO handle empty arrays being passed.
		//assertEquals(LTSDCalc.getMedian(medianEmpty), (double)5, 0.001);
	}
	
	@Test
	public void testGetMedAbsDev() {
		assertEquals(LTSDCalc.getMedAbsDev(medAbsDev1), (double)1, 0.001);
		assertEquals(LTSDCalc.getMedAbsDev(medAbsDev2), (double)1, 0.001);
		
	}
	
	@Test
	public void testHammingWindow() {
		Assert.assertArrayEquals(ltsdCalc.getHammingWindow(5), hammingWindowCheck, 0.001); 	
	}
	
	@Test
	public void testFFT() {
		LTSDCalc.doFFT(fftTest2, results);
		Assert.assertArrayEquals(results, expectedTest2, 0.001); 	
	}
	
	@Test public void testCalculateLTSDMatrixVal() {
		assertEquals(ltsdCalc.calculateLTSDMatrixVal(Sx1D, Sy1D), (double)0.0, 0.0001);
		assertEquals(ltsdCalc.calculateLTSDMatrixVal(Sx21D, Sy21D), (double)0.0289, 0.0001);
		assertEquals(ltsdCalc.calculateLTSDMatrixVal(Sx31D, Sy31D), (double)0.0393, 0.0001);	
	}
	

}

package gapStatistic;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

/**
 * @author thomaswatkins
 * 
 * This class
 *
 */
public class DataSetCollection {
	//TODO allow different windowSizes and overlapSizes between DataSets?
	ArrayList<DataSet> allDataSets = new ArrayList<DataSet>();
	static String path;
	DataSetCollection (){

	}

	DataSetCollection (ArrayList<DataSet> dataSets) {
		allDataSets = dataSets;
	}
	
	
	/**
	 * Creates a MetaDataSet of DataSet objects with their fields of: index, DataSetIdentifier, list of their seizures.
	 */
	public void initializeDataSets () {
		List<String> dataSetIdentifiers = getDataSetIdentifiers();
		System.out.println("Got identifiers");
		File directory = getDirectory();
		System.out.println("Got directory");
		List<List<String>> allDataSetsFileNames = getAllFileListsFromIdentifiers(directory, dataSetIdentifiers);
		System.out.println("Got lists of Seizures");
		//String path = getSingleOverallPath();
		System.out.println("Got path");
		ArrayList<String> paths = new ArrayList<String>();
		paths.add(path);
		int seizureOnsetSampleIndex = 5 * 60 * 200; //known format, 5 mins * 60 seconds * 200 samples per second
		int ictalStartIndex = seizureOnsetSampleIndex - 30 * 200; //go back 30 seconds in sample
		int ictalEndIndex = seizureOnsetSampleIndex + 90 * 200; //go forward 90 seconds in sample
		this.addDataSets(dataSetIdentifiers, paths, allDataSetsFileNames, ictalStartIndex, ictalEndIndex , 2000, 1000);	
	}

	/**
	 * @return
	 */
	public static File getDirectory () {
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int chooseReturnVal = chooser.showOpenDialog(null);
		if (chooseReturnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("Opening: " + chooser.getSelectedFile().getName());
		}
		File directory = chooser.getSelectedFile();
		path = directory.getAbsolutePath();
		return directory;
	}
	

	/**
	 * @return
	 */
	public List<String> getDataSetIdentifiers () {
		return FileIO.getDataSetIdentifiers();
	}

	
	/**
	 * @param directory
	 * @param identifier
	 * @return
	 */
	public List<String> getFileListFromIdentifier (File directory, String identifier) {
		return FileIO.getFileListFromIdentifier(directory, identifier);
	}

	
	/**
	 * @param directory
	 * @param identifiers
	 * @return
	 */
	public List<List<String>> getAllFileListsFromIdentifiers (File directory, List<String> identifiers) {
		List<List<String>> allFileNameLists = new ArrayList<List<String>>();
		for (String identifier : identifiers) {
			allFileNameLists.add(FileIO.getFileListFromIdentifier(directory, identifier));
		}
		printAllFileListsAllDataSets (allFileNameLists, identifiers);
		return allFileNameLists;
	}

	
	/**
	 * @param allFileNameLists
	 * @param identifiers
	 */
	private void printAllFileListsAllDataSets (List<List<String>> allFileNameLists, List<String> identifiers) {
		for (int i = 0; i < allFileNameLists.size(); i++) {
			System.out.println("DataSet index: " + i + " " + identifiers.get(i));
			for (int j = 0; j < allFileNameLists.get(i).size(); j++) {
				System.out.println("\tDataSet file: " + allFileNameLists.get(i).get(j));
			}
		}
	}
	
	
	/**
	 * @return
	 */
	public String getSingleOverallPath () {
		String path;
		JFileChooser chooser = new JFileChooser();
		int chooseReturnVal = chooser.showOpenDialog(null);
		File directory = chooser.getSelectedFile();
		if (chooseReturnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("Opening: " + chooser.getSelectedFile().getName());
		}
		//Initializing global so has path of DataSets' seizures
		path = directory.getAbsolutePath();
		return path;
	}
	
	
	/**
	 * ALL SEIZURES HAVE SAME START AND END TIMES
	 * 
	 * @param identifiers
	 * @param directoryPaths
	 * @param listOfListOfFilenames
	 * @param commonStartIndex
	 * @param commonEndIndex
	 * @param windowSize
	 * @param overlapSize
	 * @return
	 */
	public boolean addDataSets (List<String> identifiers,
			List<String> directoryPaths,
			List<List<String>> listOfListOfFilenames, 
			int commonStartIndex,
			int commonEndIndex,
			int windowSize,
			int overlapSize) {
		if( ! addDataSetsCommonChecks (identifiers, directoryPaths, listOfListOfFilenames)) {
			return false;
		}
		if (directoryPaths.size() == 1) {
			directoryPaths = duplicateDirectoryPaths (directoryPaths, identifiers.size());
		}
		for (int i = 0; i < identifiers.size(); i++) {
			if ( ! addSingleDataSetCommonChecks(identifiers.get(i),
					directoryPaths.get(i), 
					listOfListOfFilenames.get(i),
					windowSize,
					overlapSize)){
				return false;
			}
			if ( ! singleStartEndIndexChecks (commonStartIndex, commonEndIndex)) {
				return false;
			}
		}
		//Make Seizure objects
		List<List<Seizure>> ListOfSeizureLists =  new ArrayList<List<Seizure>>();
		for (int i = 0; i < listOfListOfFilenames.size(); i++) {
			ListOfSeizureLists.add(makeSeizureObjects (commonStartIndex, commonEndIndex, listOfListOfFilenames.get(i)));
		}
		for (int i = 0; i < identifiers.size(); i++) {
			int index = allDataSets.size();
			allDataSets.add(new DataSet(index, identifiers.get(i), directoryPaths.get(i), ListOfSeizureLists.get(i), windowSize, overlapSize));
		}
		return true;
	}

	
	/**
	 * ALL SEIZURES HAVE DIFFERENT START AND END TIMES!
	 * @param identifiers
	 * @param directoryPaths
	 * @param listOfListOfFilenames
	 * @param listStartIndices
	 * @param listEndIndices
	 * @param windowSize
	 * @param overlapSize
	 * @return
	 */
	public boolean addDataSets (List<String> identifiers,
			List<String> directoryPaths,
			List<List<String>> listOfListOfFilenames, 
			List<int[]> listStartIndices,
			List<int[]> listEndIndices,
			int windowSize,
			int overlapSize) {
		addDataSetsCommonChecks(identifiers, directoryPaths, listOfListOfFilenames);
		int commonSize = identifiers.size();
		if (listStartIndices.size() != commonSize) {
			System.out.println("Not the same number of lists of start indices as there are DataSet identifiers");
			return false;
		}
		if (listEndIndices.size() != commonSize) {
			System.out.println("Not the same number of lists of end indices as there are DataSet identifiers");
			return false;	
		}
		if (directoryPaths.size() == 1) {
			directoryPaths = duplicateDirectoryPaths(directoryPaths, identifiers.size());
		}
		for (int i = 0; i < identifiers.size(); i++) {
			if ( ! addSingleDataSetCommonChecks(identifiers.get(i),
					directoryPaths.get(i), 
					listOfListOfFilenames.get(i),
					windowSize,
					overlapSize)){
				return false;
			}
			if ( ! multipleStartEndIndicesChecks(listStartIndices.get(i), listEndIndices.get(i), listOfListOfFilenames.get(i).size())){
				return false;
			}	
		}
		//Make Seizure objects
		List<List<Seizure>> ListOfSeizureLists =  new ArrayList<List<Seizure>>();
		for (int i = 0; i < listOfListOfFilenames.size(); i++) {
			ListOfSeizureLists.add(makeSeizureObjects(listStartIndices.get(i), listEndIndices.get(i), listOfListOfFilenames.get(i)));
		}
		for (int i = 0; i < identifiers.size(); i++) {
			int index = allDataSets.size();
			allDataSets.add(new DataSet(index, identifiers.get(i), directoryPaths.get(i), ListOfSeizureLists.get(i), windowSize, overlapSize));
		}
		return true;
	}
	
	
	private List<String> duplicateDirectoryPaths (List<String> directoryPaths, int numDataSets) {
		String directoryPath =  directoryPaths.get(0);
		directoryPaths.clear();
		for(int i = 0; i < numDataSets; i++) {
			directoryPaths.add(directoryPath);
		}
		return directoryPaths;
	}

	
	/**
	 * Factory method SAME START AND END TIMES
	 * @param identifier, unique identifier that can be used to find all seizure files in its directory
	 * @param directoryPath, filepath to directory containing all its seizures files
	 * @param szFileNames, all names of the files which contain seizure information
	 * @param startIndices, start indices of all seizures in same order as they are in the szFileName list.
	 * @param endIndices, end indices of all seizures in same order as they are in the szFileName list.
	 * @param windowSize, size of window to be used by short time fourier transform to analyze the seizure
	 * @param overlapSize, amount by which windows will overlap in the seizure data.
	 * @return boolean indicating DataSet could be added.
	 */
	public boolean addDataSetToDatasetCollection (String identifier,
			String directoryPath, 
			List<String> szFileNames,
			int commonStartIndex,//This will be annoying, maybe adjust?
			int commonEndIndex,
			int windowSize,
			int overlapSize
			) {
		//Checking
		if ( ! addSingleDataSetCommonChecks(identifier,
				directoryPath, 
				szFileNames,
				windowSize,
				overlapSize)){
			return false;
		}
		if ( ! singleStartEndIndexChecks(commonStartIndex, commonEndIndex)) {
			return false;
		}
		//Make seizure objects
		ArrayList<Seizure> seizureList = makeSeizureObjects(commonStartIndex, commonEndIndex, szFileNames);
		int index = allDataSets.size();
		allDataSets.add(new DataSet(index, identifier, directoryPath, seizureList, windowSize, overlapSize));
		return true;
	}
	
	
	/**
	 * Factory method DIFFERENT START AND END TIMES
	 * @param identifier, unique identifier that can be used to find all seizure files in its directory
	 * @param directoryPath, filepath to directory containing all its seizures files
	 * @param szFileNames, all names of the files which contain seizure information
	 * @param startIndices, start indices of all seizures in same order as they are in the szFileName list.
	 * @param endIndices, end indices of all seizures in same order as they are in the szFileName list.
	 * @param windowSize, size of window to be used by short time fourier transform to analyze the seizure
	 * @param overlapSize, amount by which windows will overlap in the seizure data.
	 * @return boolean indicating DataSet could be added.
	 */
	public boolean addDataSetToDatasetCollection (String identifier,
			String directoryPath, 
			List<String> szFileNames,
			int[] startIndices,//This will be annoying, maybe adjust?
			int[] endIndices,
			int windowSize,
			int overlapSize
			) {
		//Checking
		if ( ! addSingleDataSetCommonChecks(identifier, directoryPath, szFileNames, windowSize, overlapSize)){
			return false;
		}
		if ( ! multipleStartEndIndicesChecks(startIndices, endIndices, szFileNames.size())){
			return false;
		}
		//make Seizure objects
		ArrayList<Seizure> seizureList = makeSeizureObjects(startIndices, endIndices, szFileNames);
		int index = allDataSets.size();
		allDataSets.add(new DataSet(index, identifier, directoryPath, seizureList, windowSize, overlapSize));
		return true;
	}
	
	
	/**
	 * Helper for factory method SAME START AND END TIMES
	 * @param commonStartIndex
	 * @param commonEndIndex
	 * @param szFileNames
	 * @return
	 */
	private ArrayList<Seizure> makeSeizureObjects (int commonStartIndex, int commonEndIndex, List<String> szFileNames){
		ArrayList<Seizure> seizureList = new ArrayList<Seizure>();
		for(int j = 0; j < szFileNames.size(); j++) {
			seizureList.add(new Seizure(commonStartIndex, commonEndIndex, szFileNames.get(j)));
		}
		return seizureList;
	}
	
	
	/**
	 * Helper for factory method DIFFERENT START AND END TIMES
	 * @param startIndices
	 * @param endIndices
	 * @param szFileNames
	 * @return
	 */
	private ArrayList<Seizure> makeSeizureObjects (int[] startIndices, int[] endIndices, List<String> szFileNames){
		ArrayList<Seizure> seizureList = new ArrayList<Seizure>();
		for(int j = 0; j < szFileNames.size(); j++) {
			seizureList.add(new Seizure(startIndices[j], endIndices[j], szFileNames.get(j)));
		}
		return seizureList;
	}
	
	
	/**
	 * @param identifiers
	 * @param directoryPaths
	 * @param listOfListOfFilenames
	 * @return
	 */
	private boolean addDataSetsCommonChecks (List<String> identifiers,
			List<String> directoryPaths,
			List<List<String>> listOfListOfFilenames 
			) {
		if (directoryPaths.size() < 1) {
			System.out.println("No directories listed, nowhere to get files from");
			return false;
		}
		int commonSize = identifiers.size();
		if (commonSize == 0) {
			System.out.println("No DataSet identifiers given");
			return false;
		}
		if (directoryPaths.size() != 1 || directoryPaths.size() != commonSize) {
			System.out.println("Neither a single directory or the same number of directories as DataSet identifiers given");
			return false;
		}
		if (listOfListOfFilenames.size() != commonSize) {
			System.out.println("Not the same number of lists of seizure file names as there are DataSet identifiers");
			return false;
		}
		return true;

	}

	
	/**
	 * @param identifier
	 * @param directoryPath
	 * @param szFileNames
	 * @param windowSize
	 * @param overlapSize
	 * @return
	 */
	private boolean addSingleDataSetCommonChecks (String identifier,
			String directoryPath, 
			List<String> szFileNames,
			int windowSize,
			int overlapSize) {
		if (szFileNames.size() < 2) {
			System.out.println("Num seizure files: " + szFileNames.size() + "minimum 2 seizure files reuired to produce meaningful LTSD matrix");
		}
		for (DataSet dataSet: allDataSets) {
			if (dataSet.identifier.equals(identifier)) {
				System.out.println("DataSet with this identifier already present in dataSetCollection");
				return false;
			}
			if (dataSet.filePath.equals(directoryPath)) {
				for (Seizure seizure: dataSet.seizures) {
					if(szFileNames.contains(seizure.fileName)) {
						System.out.println("DataSet in dataset whose files are in the same directory: " + directoryPath + " already using the \n" +
								"file: " + seizure.fileName + ". Can't add this DataSet: " + identifier);
						return false;
					}
				}
			}
		}
		if (overlapSize < 1 || windowSize < 2) {
			System.out.println("Either overlap size or window size too small");
			return false;
		}
		if (overlapSize >= windowSize) {
			System.out.println("Overlap greater than or equal to windowsize");
			return false;
		}
		return true;
	}

	
	/**
	 * @param commonStartIndex
	 * @param commonEndIndex
	 * @return
	 */
	public boolean singleStartEndIndexChecks (int commonStartIndex, int commonEndIndex) {
		if(commonStartIndex < 0 || commonEndIndex < 0) {
			System.out.println("Either start or end index contains a negative number, illegal for an index");
			return false;
		}
		int difference = commonEndIndex - commonStartIndex;
		if (difference < 1) {
			System.out.println("Error start index larger than end index!");
			return false;
		}
		return true;
	}

	
	/**
	 * @param startIndices
	 * @param endIndices
	 * @param numSeizures
	 * @return
	 */
	public boolean multipleStartEndIndicesChecks (int[] startIndices, int[] endIndices, int numSeizures) {
		if( ! singleStartEndIndexChecks(startIndices[0], endIndices[0])) {
			return false;
		}
		int difference = endIndices[0] - startIndices[0];
		for (int i = 0; i < startIndices.length; i++) {
			if (endIndices[i] - startIndices[i] != difference) {
				System.out.println("Length of time seizures being analyzed over differs");
				return false;
			}
			if(startIndices[i] < 0 || endIndices[i] < 0) {
				System.out.println("Either start or end indices at " + i + " contains a negative number, illegal for an index");
			}
		}
		if (startIndices.length != endIndices.length ) {
			System.out.println("Number of startIndices and endIndices do not match."); 
			return false;
		}
		if (startIndices.length != numSeizures) {
			System.out.println("Number of start and end indices do not mach number of seizure files"); 
			return false;
		}
		return true;

	}

	
	public boolean removeDataSetFromDatasetCollection (String identifier) {
		//TODO
		return false;
	}

	public boolean removeDataSetFromDatasetCollection (int index) {
		//TODO
		return false;
	}









	
	
	
	
}
